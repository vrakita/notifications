<?php

namespace Notifications;

abstract class Sender {

	/**
	 * Sender address
	 *
	 * @var string $from
	 *
	 */
	protected $from;

	/**
	 * Recipient address
	 *
	 * @var string $to
	 *
	 */
	protected $to;

	/**
	 * Message to be sent
	 *
	 * @var string $to
	 *
	 */
	protected $message;

	/**
	 * Subject of message
	 *
	 * @var string $subject
	 *
	 */
	protected $subject;

	/**
	 * Set $from
	 *
	 * @param string $from
	 * @return $this
	 *
	 */
	public function setFrom($from) {
		$this->from = $from;

		return $this;
	}

	/**
	 * Set address for message to be sent
	 *
	 * @param string $to
	 * @return $this
	 *
	 */
	public function setTo($to) {
		$this->to = $to;

		return $this;
	}

	/**
	 * Set message to be sent
	 *
	 * @param string $message
	 * @return $this
	 *
	 */
	public function setMessage($message) {
		$this->message = $message;

		return $this;
	}

	/**
	 * Set subject of message to be sent
	 *
	 * @param string $subject
	 * @return $this
	 *
	 */
	public function setSubject($subject) {
		$this->subject = $subject;

		return $this;
	}

	/**
	 * Set config array
	 *
	 * @param array $config
	 * @return $this
	 *
	 */
	public function setConfig($config) {
		$this->config = $config;

		return $this;
	}

	/**
	 * Sending message
	 *
	 * @return mixed
	 * @throws \Exception
	 *
	 */
	public abstract function send();



}