<?php

namespace Notifications\Services;

use Notifications\Sender;

use Services_Twilio;

class SMSTwilio extends Sender {

	public function send() {

		$this->validation();

		$provider = new Services_Twilio($this->config['account_sid'], $this->config['auth_token']);

		try {

			$message = $provider->account->messages->create([
			    "From" 	=> $this->from, // From a valid Twilio number
			    "To" 	=> $this->to,   // Text this number
			    "Body" 	=> $this->message,
		    ]);

		    return $message->sid;

		} catch(\Exception $e) {
			throw new \Exception($e->getMessage());
		}

	}

	protected function validation() {

		if( ! isset($this->config['account_sid'], $this->config['auth_token'])) throw new \Exception ('Twilio credentials are missing');

		if( ! isset($this->to, $this->from, $this->message)) throw new \Exception ('Required information for sending sms is missing');

	}


}