<?php

namespace Notifications\Services;

use Notifications\Sender;

class SendGridEmail extends Sender {


	public function send() {

		$this->validation();

		$from = new \SendGrid\Email(null, $this->from);
		$subject = $this->subject;
		$to = new \SendGrid\Email(null, $this->to);
		$content = new \SendGrid\Content("text/plain", $this->message);
		$mail = new \SendGrid\Mail($from, $subject, $to, $content);

		$apiKey = $this->config['SENDGRID_API_KEY'];
		$sg = new \SendGrid($apiKey);

		try {

			return $sg->client->mail()->send()->post($mail);

		} catch(\Excetpion $e) {

			throw new Exception($e->getMessage);
			
		}		
		
	}

	protected function validation() {

		if( ! isset($this->config['SENDGRID_API_KEY'])) throw new \Exception ('SendGrid API KEY is missing');

		if( ! isset($this->to, $this->from, $this->message)) throw new \Exception ('Required information for sending email is missing');

	}

}